/**
 * Created by ahadrii on 31/08/16.
 */

var Subject = require('../models/subject').Subject;

var searchService = function() {

    var searchSubject = function(query, cb) {
        Subject.find({label: {$regex: query, $options: 'i'}}).limit(10).exec(function(err, subjects) {
            if (err) {
              cb(new Error(err.message));
            } else {
              cb(null, subjects);
            }
          });
      };
    return {
            searchSubject: searchSubject
          };
  };
module.exports = searchService;
