/**
 * Created by ahadrii on 04/08/16.
 */

var Student = require('../models/student').Student,
    jwt = require('jwt-simple'),
    config = require('../config/db.config'),
    pino = require('pino')(), // Logger
    Professor = require('../models/professor').Professor;

var authService = function() {

    var checkAccount = function(email, cb) {
        Professor.findOne({
          email: email
        }, function(err, professor) {
          if (err) {
            cb(new Error(err.message));
            return ;
          }
          if (!professor) {
            Student.findOne({
              email: email
            }, function(err, student) {
              if (err) {
                cb(new Error(err.message));
                return ;
              }
              if (!student) {
                cb(false, true);
              } else {
                cb(new Error('Compte deja existe avec ce email'));
              }
            });
          } else {
            cb(new Error('Compte deja existe avec ce email'));
          }
        });
      };

    var authUser = function(req, cb) {
        Professor.findOne({
            email: req.body.email,
          }, function(err, professor) {
            if (err) {
              cb(new Error(err.message));
            }
            if (!professor) {
              Student.findOne({
                  email: req.body.email
                }, function(err, student) {
                  if (err) {
                    cb(new Error(err.message));
                    return ;
                  }
                  if (!student) {
                    cb(new Error('Compte non trouve, Svp verifier votre mot de passe ou adresse email.'));
                  } else {
                    student.comparePassword(req.body.password, function(err, result) {
                        if (err) {
                          cb(new Error('Compte non trouve, Svp verifier votre mot de passe ou adresse email.'));
                        } else {
                          cb(null, student);
                        }
                      });
                  }
                });
            } else {
              professor.comparePassword(req.body.password, function(err, result) {
                if (err) {
                  cb(new Error('Compte non trouve, Svp verifier votre mot de passe ou adresse email.'));
                } else {
                  cb(null, professor);
                }
              });
            }
          });
      };

    var findUserById = function(_id, cb) {
        Professor.find({
            _id: _id
          }, function(err, professor) {
            if (err) {
              cb(new Error(err.message));
            }
            if (!professor) {
              Student.findOne({
                _id: _id
              }, function(err, student) {
                if (err) {
                  cb(new Error(err.message));
                  return ;
                }
                if (!student) {
                  cb(new Error('Compte non trouve, Svp verifier votre id.'));
                } else {
                  cb(null, student);
                }
              });
            } else {
              cb(null, professor);
            }
          });
      };

    return {
      checkAccount: checkAccount,
      authUser: authUser,
      findUserById: findUserById
    };
  };
module.exports = authService;
