/**
 * Created by ahadrii on 14/08/16.
 */

var Cycle = require('../models/cycle').Cycle;

var cycleService = function() {



      var addLevel = function(object, cb) {
            Cycle.findByIdAndUpdate({_id: object._id}, {$push: {'_levels': object._levelId}}, {safe: true, upsert: false, new: true}, function(err, cycle) {
                if (err) {
                  cb(new Error(err.msg));
                } else {
                  cb(null, cycle);
                }
              });
          };

      function findCycle(label, cb) { // For post purpose
        Cycle.findOne({
            label: label
          }, function(err, cycle) {
            if (err) {
              cb(new Error(err.message));
              return ;
            }
            if (!cycle) {
              cb(false, true);
            } else {
              cb(new Error('Un cycle existe déjà avec le même intitule'));
            }
          });
      }

      var saveCycle = function(req, cb) {
          findCycle(req.body.label, function(err) {
              if (err) {
                cb(new Error(err.message));
              } else {
                var newCycle = new Cycle({
                    label: req.body.label,
                    _professors: req.body._professors,
                    _levels: req.body._levels,
                    _subjects: req.body._subjects
                  });
                newCycle.save(function(err) {
                    if (err) {
                      cb(new Error(err.message));
                    } else {
                      cb(null, newCycle);
                    }
                  });
              }
            });
        };

      var getAllCycles = function(cb) {
          Cycle.find({}).populate({path: '_levels'}).exec(function(err, cycles) {
              if (err) {
                cb(new Error(err.message));
                return ;
              }
              if (!cycles) {
                cb(null, false);
              } else {
                cb(null, cycles);
              }
            });
        };

      var getCycleById = function(_id, cb) {
          Cycle.findOne({
              _id: _id
            }).populate({path: '_levels', populate: {path: '_specialities', populate: {path: '_subjects'}}}).exec(function(err, cycle) {
              if (err) {
                cb(new Error(err.message));
                return ;
              }
              if (!cycle) {
                cb(null, false);
              } else {
                cb(null, cycle);
              }
            });
        };

      var delCycleById = function(_id, cb) {
          Cycle.remove({_id: _id}, function(err) {
              if (err) {
                cb(new Error(err.msg));
              } else {
                cb(null, 'Cycle removed successfully');
              }
            });
        };

      var updateCycle = function(req, cb) {
          Cycle.findByIdAndUpdate({_id: req.body._id}, {$set: {_levels: req.body._levels, _subjects: req.body._subjects},
              label: req.body.label}, {safe: true, upsert: true, new: true}, function(err, cycle) {
              if (err) {
                cb(new Error(err.msg));
              } else {
                cb(null, cycle);
              }
            });
        };

      var delAllCycles = function(cb) {
          Cycle.remove({}, function(err) {
              if (err) {
                cb(new Error(err.msg));
              } else {
                cb(null, 'All Cycles removed successfully');
              }
            });
        };

      return {
          addLevel: addLevel,
          saveCycle: saveCycle,
          getAllCycles: getAllCycles,
          getCycleById: getCycleById,
          delCycleById: delCycleById,
          updateCycle: updateCycle,
          delAllCycles: delAllCycles
        };
    };

module.exports = cycleService;
