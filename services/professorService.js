/**
 * Created by ahadrii on 26/08/16.
 */

var Professor = require('../models/professor').Professor,
    authService = require('../services/authService')();

var professorService = function() {

    var countProfessors = function(cb) {
        Professor.count({}, function(err, count) {
            if (err) {
              return cb(new Error(err.message));
            } else {
              return cb(null, count);
            }
          });
      };

    var addSubject = function(object, cb) {
        Professor.findByIdAndUpdate({_id: object._id}, {$push: {'_subjects': object._subjectId}}, {safe: true, upsert: false, new: true}).populate({path: '_subjects'})
            .populate({path: '_sessions'}).exec(function(err, professor) {
            if (err) {
              cb(new Error(err.msg));
            } else {
              cb(null, professor);
            }
          });
      };

    var updateProfessor = function(req, cb) {
        Professor.findOne({
            _id: req.body._id
          }, function(err, professor) {
            if (err) {
              cb(new Error('Site en maintenance, Svp essayer ultérieurement'));
            }
            if (!professor) {
              cb(new Error('Professeur non trouvé, Svp vérifier vos informations'));
            } else {
              console.log(req.body._subjects);
              professor.fname =  req.body.fname;
              professor.lname =  req.body.lname;
              professor.email =  req.body.email;
              professor.date_birth = req.body.date_birth;
              professor.phone = req.body.phone;
              professor.profession = req.body.profession;
              professor.desc = req.body.desc ;
              professor.status = req.body.status ;
              professor._subjects = [];
              if (req.body._subjects) {
                var subject = {};
                for (subject of req.body._subjects) {
                  professor._subjects.push(subject);
                }
              }
              if (req.body._sessions) {
                professor._sessions.push(req.body._sessions);
              }
              professor.phone = req.body.phone;
              professor.save();
              cb(null, professor);
            }
          });
      };

    var updateProfessorStatus = function(req, cb) {
        Professor.findOneAndUpdate({_id: req.body._id}, {$set: {status: req.body.status}}, {new: true}).populate({path: '_subjects'})
            .populate({path: '_sessions'}).exec(function(err, professor) {
            if (err) {
              cb(new Error('Site en maintenance, Svp essayer ultérieurement'));
            } else {
              cb(null, professor);
            }
          });
      };

    var saveProfessor = function(req, cb) {
        authService.checkAccount(req.body.email, function(err) {
            if (err) {
              cb(new Error(err.message));
            } else {
              var newProfessor = new Professor({
                  fname: req.body.fname,
                  lname: req.body.lname,
                  email: req.body.email,
                  password: req.body.password
                });
              newProfessor.save(function(err) {
                  if (err) {
                    cb(new Error(err.message));
                  } else {
                    cb(null, 'Compte crée');
                  }
                });
            }
          });
      };

    var findProfessorById = function(_id, cb) {
        Professor.findOne({
            _id: _id
          }, function(err, professor) {
            if (err) {
              cb(new Error(err.message));
            }
            if (!professor) {
              cb(new Error('Professeur non trouver, Svp vérifier vos informations'));
            } else {
              cb(null, professor);
            }
          });
      };
    var getAllProfessors = function(cb) {
      Professor.find({}).populate({path: '_subjects'}).exec(function(err, professors) {
          if (err) {
            cb(new Error(err.message));
            return;
          }
          if (!professors) {
            cb(null, false);
          } else {
            cb(null, professors);
          }
        });
    };

    return {
        countProfessors: countProfessors,
        addSubject: addSubject,
        saveProfessor: saveProfessor,
        updateProfessor: updateProfessor,
        updateProfessorStatus: updateProfessorStatus,
        findProfessorById: findProfessorById,
        getAllProfessors: getAllProfessors
      };
  };

module.exports = professorService;
