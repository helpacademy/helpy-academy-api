/**
 * Created by ahadrii on 13/08/16.
 */

var Subject = require('../models/subject').Subject;

var subjectService = function() {

      function findSubject(label, cb) {
        Subject.findOne({
          label: label
        }, function(err, subject) {
          if (err) {
            cb(new Error(err.message));
            return ;
          }
          if (!subject) {
            cb(false, true);
          } else {
            console.log(subject);
            cb(new Error('Une matiere existe déjà avec le même intitule'));
          }
        });
      }

      var saveSubject = function(req, cb) {
          findSubject(req.body.label, function(err) {
            if (err) {
              cb(new Error(err.message));
            } else {
              var newSubject = new Subject({
                  label: req.body.label,
                  _level: req.body._level,
                  _professors: req.body._professors
                });
              newSubject.save(function(err) {
                  if (err) {
                    cb(new Error(err.message));
                  } else {
                    cb(null, newSubject);
                  }
                });
            }
          });
        };

      var getAllSubjects = function(cb) {
          Subject.find({}).populate({path: '_level'}).populate({path: '_cycle'}).exec(function(err, subjects) {
              if (err) {
                cb(new Error(err.msg));
              } else {
                cb(null, subjects);
              }
            });
        };

      var getSubjectById = function(_id, cb) {
          Subject.find({_id: _id}, function(err, subject) {
              if (err) {
                cb(new Error(err.msg));
              } else if (!subject) {
                cb(new Error('No Subject found'));
              } else {
                cb(null, subject);
              }
            });
        };

      var delSubjectById = function(_id, cb) {
          Subject.remove({_id: _id}, function(err) {
              if (err) {
                cb(new Error(err.msg));
              } else {
                cb(null, 'Subject removed successfully');
              }
            });
        };

      var updateSubjectById = function(req, cb) {
          Subject.findByIdAndUpdate({_id: req.body._id}, {
              label: req.body.label, _level: req.body._level, _cycle: req.body._cycle, $set: {_professors: req.body._professors}}, {safe: true, upsert: true, new: true}, function(err, subject) {
              if (err) {
                cb(new Error(err.msg));
              } else {
                cb(null, subject);
              }
            });
        };

      var delAllSubjects = function(cb) {
          Subject.remove({}, function(err) {
              if (err) {
                cb(new Error(err.msg));
              } else {
                cb(null, 'Subjects removed successfully');
              }
            });
        };

      return {
          saveSubject: saveSubject,
          getAllSubjects: getAllSubjects,
          getSubjectById: getSubjectById,
          delSubjectById: delSubjectById,
          updateSubjectById: updateSubjectById,
          delAllSubjects: delAllSubjects
        };
    };

module.exports = subjectService;
