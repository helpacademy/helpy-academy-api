/**
 * Created by ahadrii on 26/08/16.
 */

var Student = require('../models/student').Student,
    authService = require('../services/authService')();

var studentService = function() {

    var countStudent = function(cb) {
        Student.count({}, function(err, count) {
            if (err) {
              cb(new Error(err.message));
            } else {
              cb(null, count);
            }
          });
      };

    var updateStudent = function(req, cb) {
        Student.findOne({
            _id: req.body._id
          }, function(err, student) {
            if (err) {
              cb(new Error('Serveur en maintenance, Svp essayer ultérieurement'));
            }
            if (!student) {
              cb(new Error('Etudiant non trouvé, Svp vérifier vos informations'));
            } else {
              student.fname =  req.body.fname;
              student.lname =  req.body.lname;
              student.email =  req.body.email;
              student.date_birth = req.body.date_birth;
              student.phone = req.body.phone;
              student.profession = req.body.profession;
              student.desc = req.body.desc ;
              student.phone = req.body.phone;
              student.save();
              cb(null, student);
            }
          });
      };

    var saveStudent = function(req, cb) {
        authService.checkAccount(req.body.email, function(err) {
            if (err) {
              cb(new Error(err.message));
            } else {
              var newStudent = new Student({
                  fname: req.body.fname,
                  lname: req.body.lname,
                  email: req.body.email,
                  password: req.body.password
                });
              newStudent.save(function(err) {
                  if (err) {
                    cb(new Error(err.message));
                  } else {
                    cb(null, 'Compte crée');
                  }
                });
            }
          });
      };

    var findStudentById = function(_id, cb) {
        Student.findOne({
            _id: _id
          }, function(err, student) {
            if (err) {
              cb(new Error(err.message));
            }
            if (!student) {
              cb(new Error('Student non trouver, Svp vérifier vos informations'));
            } else {
              cb(null, student);
            }
          });
      };
    var getAllStudents = function(cb) {
      Student.find({}, function(err, students) {
          if (err) {
            cb(new Error(err.message));
          } else {
            cb(null, students);
          }
        });
    };

    return {
        countStudent: countStudent,
        saveStudent: saveStudent,
        updateStudent: updateStudent,
        findStudentById: findStudentById,
        getAllStudents: getAllStudents
      };
  };

module.exports = studentService;
