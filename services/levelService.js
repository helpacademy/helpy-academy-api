/**
 * Created by ahadrii on 14/08/16.
 */

var Level = require('../models/level').Level;

var levelService = function() {

    var searchFor = function(query, cb) {
        Level.find({label: 'fff'}, function(err, levels) {
            console.log('Test');
            if (err) {
              console.log(err.message);
            } else {
              console.log(levels);
            }
          });
      };

    var countLevels = function(cb) {
        Level.count({}, function(err, count) {
            if (err) {
              cb(new Error(err.message));
            } else {
              cb(null, count);
            }
          });
      };

    var addSubject = function(object, cb) {
        Level.findByIdAndUpdate({_id: object._id}, {$push: {'_subjects': object._subjectId}}, {safe: true, upsert: false, new: true}, function(err, level) {
            if (err) {
              cb(new Error(err.msg));
            } else {
              cb(null, level);
            }
          });
      };

    var findLevel = function(label, cb) { // For post purpose
        Level.findOne({
            label: label
          }, function(err, level) {
            if (err) {
              cb(new Error(err.message));
              return ;
            }
            if (!level) {
              cb(false, true);
            } else {
              cb(new Error('Un niveau existe déjà avec le même intitule'));
            }
          });
      };

    var saveLevel = function(req, cb) {
        findLevel(req.body.label, function(err) {
            if (err) {
              cb(new Error(err.message));
            } else {
              var newLevel = new Level({
                  label: req.body.label,
                  _cycle: req.body._cycle,
                  _subjects: req.body._subjects
                });
              newLevel.save(function(err) {
                  if (err) {
                    cb(new Error(err.message));
                  } else {
                    cb(null, newLevel);
                  }
                });
            }
          });
      };

    var getAllLevels = function(cb) {
        Level.find({}).populate({path: '_subjects'}).populate({path: '_cycle'}).exec(function(err, levels) {
            if (err) {
              cb(new Error(err.message));
              return;
            }
            if (!levels) {
              cb(null, false);
            } else {
              cb(null, levels);
            }
          });
      };

    var getLevelById = function(_id, cb) {
        Level.findOne({
            _id: _id
          }).populate({path: '_subjects'}).exec(function(err, level) {
            if (err) {
              cb(new Error(err.message));
              return ;
            }
            if (!level) {
              cb(null, false);
            } else {
              cb(null, level);
            }
          });
      };

    var delLevelById = function(_id, cb) {
        Level.remove({_id: _id}, function(err) {
            if (err) {
              cb(new Error(err.msg));
            } else {
              cb(null, 'Level removed successfully');
            }
          });
      };

    var updateLevel = function(req, cb) {
        Level.findByIdAndUpdate({_id: req.body._id}, {$set: {_subjects: req.body._subjects},
            label: req.body.label, _cycle: req.body._cycle}, {safe: true, upsert: false, new: true}, function(err, level) {
            if (err) {
              cb(new Error(err.msg));
            } else {
              cb(null, level);
            }
          });
      };

    var delAllLevels = function(cb) {
        Level.remove({}, function(err) {
            if (err) {
              cb(new Error(err.msg));
            } else {
              cb(null, 'All Levels removed successfully');
            }
          });
      };

    return {
        searchFor: searchFor,
        countLevels: countLevels,
        findLevel: findLevel,
        saveLevel: saveLevel,
        getAllLevels: getAllLevels,
        getLevelById: getLevelById,
        delLevelById: delLevelById,
        updateLevel: updateLevel,
        delAllLevels: delAllLevels,
        addSubject: addSubject
      };
  };

module.exports = levelService;
