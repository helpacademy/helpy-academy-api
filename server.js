var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var multer = require('multer');
var isAdmin = require('./middlewares/adminMiddleware');

// multer config for the local storage
var storage = multer.diskStorage({
    destination:  './uploads/',
    filename: function(req, file, callback) {
        callback(null, file.originalname);
      }
  });

var upload = multer({storage: storage}).single('profilePic');

/**
 * Config
 */
var port = process.env.PORT || 3000;
var app = express();

// DataBase
var configDB = require('./config/db.config');
mongoose.connect(configDB.url); // connect to the database
require('./models/session');

// Config passport
require('./routes/config/passport')(passport);
/**
 * Middleware
 */
// Log to console
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
  }));

app.use(express.static('./uploads'));

//Enabling CROSS
app.use('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Max-Age', '86400'); // 24 hours
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
    if (req.method === 'OPTIONS') { // Sick woman!
      res.end();
    } else {
      next();
    }
  });
// Password
app.use(passport.initialize());

/**
 * Routing
*/
// Routes
var index = require('./routes/index')(port);
var auth = require('./routes/auth')(passport);
var professors = require('./routes/professor')(passport);
var students = require('./routes/student')(passport);
var subjects = require('./routes/subject')(passport);
var levels = require('./routes/level')(passport);
var cycles = require('./routes/cycle')(passport);
var search = require('./routes/search')();

app.use('/', index);
app.use('/api/auth', auth);
app.use('/api/students', students);
app.use('/api/professors', professors);
//app.use(['/api/subjects'],isAdmin); /** Check if the user is an admin ; **/
app.use('/api/subjects', subjects);
app.use('/api/levels', levels);
app.use('/api/cycles', cycles);
app.use('/api/search', search);

/** Uploading  image profile **/
app.post('/api/profilephoto', function(req, res) {
    upload(req, res, function(err) {
        if (err) {
          return res.json({success: false, msg: err.msg});
        }
        res.json({success: true, msg: 'Image uploaded successfully'});
      });
  });

/**
 * Lunching the app
 */
app.listen(port, function() {
  console.log('Server running on localhost:' + port);
});
