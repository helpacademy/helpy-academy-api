/**
 * Created by ahadrii on 14/08/16.
 */

var config = require('../config/db.config'),
    jwt = require('jwt-simple');

var adminMiddleware = function(req, res, next) {
    var token = getToken(req.headers);
    console.log(token);
    if (token) {
      var decodedUser = jwt.decode(token, config.secret);
      if (decodedUser.isAdmin) {
        next();
      } else {
        return next(new Error('You are not authorized for this action'));
      }
    }else {
      return next(new Error('No token provided'));
    }
  };

function getToken(headers) {
  console.log(headers);
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

module.exports = adminMiddleware;
