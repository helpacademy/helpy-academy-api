/**
 * Created by ahadrii on 30/07/16.
 */

var express = require('express'),
    jwt = require('jwt-simple'),
    authRouter = express.Router(),
    config = require('../config/db.config'),
    authService = require('../services/authService')();

var router = function(passport) {
    authRouter.route('/login')
        .post(function(req, res) {
            if (!req.body.email || !req.body.password) {
              res.json({success: false, msg: 'Svp entrer votre mot de passe et adresse email'});
            } else {
              authService.authUser(req, function(err, user) {
                  if (err) {
                    res.json({success: false, msg: err.message});
                  }else {
                    var token = jwt.encode(user, config.secret);
                    res.json({success: true, user: user, token: 'JWT ' + token});
                  }
                });
            }
          });

    return authRouter;
  };

module.exports = router;

