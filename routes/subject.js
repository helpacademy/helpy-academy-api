/**
 * Created by ahadrii on 13/08/16.
 */

var express = require('express'),
    subjectRouter = express.Router(),
    subjectService = require('../services/subjectService')();

var router = function(passport) {
    subjectRouter.route('/')
        .post(passport.authenticate('jwt', {session: false}), function(req, res) { /** create a subject route's **/
                      // Inserting the subject into the database
                      subjectService.saveSubject(req, function(err, result) {
                          if (err) {
                            res.json({success: false, msg: err.message});
                          } else {
                            res.json({success: true, msg: result});
                          }
                        });
                    })
        .get(passport.authenticate('jwt', {session: false}), function(req, res) { /** Get all the subjects **/
            subjectService.getAllSubjects(function(err, subjects) {
                console.log(req.body);
                if (err) {
                  res.json({success: false, msg: err.message});
                } else {
                  res.json({success: true, subjects: subjects});
                }
              });
          })
        .put(passport.authenticate('jwt', {session: false}), function(req, res) { /** update subject by it's _id **/
          subjectService.updateSubjectById(req, function(err, subject) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, subject: subject});
              }
            });
        })
        .delete(passport.authenticate('jwt', {session: false}), function(req, res) { /** Delete all subjects ==> i didn't test it, im lazy to insert all the subjects again, but trust me it works**/
            subjectService.delAllSubjects(function(err, result) {
                if (err) {
                  res.json({success: false, msg: err.message});
                } else {
                  res.json({success: true, msg: result});
                }
              });
          });

    /** Get 1 subject by it's _id  **/
    subjectRouter.route('/:_id')
        .get(passport.authenticate('jwt', {session: false}), function(req, res) {
            var _id = req.params._id ;
            subjectService.getSubjectById(_id, function(err, subject) {
                if (err) {
                  res.json({success: false, msg: err.message});
                } else {
                  res.json({success: true, subject: subject});
                }
              });
          })
        .delete(passport.authenticate('jwt', {session: false}), function(req, res) { /** Delete subject by it's _id **/
            var _id = req.params._id ;
            subjectService.delSubjectById(_id, function(err, result) {
                if (err) {
                  res.json({success: false, msg: err.message});
                } else {
                  res.json({success: true, msg: result});
                }
              });
          });

    return subjectRouter;
  };

module.exports = router;

