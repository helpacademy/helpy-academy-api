/**
 * Created by ahadrii on 26/08/16.
 */

var express = require('express'),
    jwt = require('jwt-simple'),
    studentRouter = express.Router(),
    config = require('../config/db.config'),
    studentService = require('../services/studentService')();

var router = function(passport) {
    studentRouter.route('/')
        .put(passport.authenticate('jwt', {session: false}), function(req, res) { // update a professor route's
            studentService.updateStudent(req, function(err, updatedStudent) {
                if (err) {
                  return res.json({success: false, msg: err.message});
                } else {
                  var token = jwt.encode(updatedStudent, config.secret);
                  return res.json({success: true, user: updatedStudent, token: 'JWT ' + token});
                }
              });
          })
        .post(function(req, res) { // The SignUp route
            if (!req.body.email || !req.body.password || !req.body.fname || !req.body.lname) {
              res.json({success: false, msg: 'S\'il vous plaît remplir tous les champs .'});
            } else {
              studentService.saveStudent(req, function(err, result) {
                  if (err) {
                    return res.json({success: false, msg: err.message});
                  } else {
                    res.json({success: true, msg: result});
                  }
                });
            }
          })
        .get(passport.authenticate('jwt', {session: false}), function(req, res) { /** Get all the students **/
          studentService.getAllStudents(function(err, students) {
              if (err) {
                res.json({success: false, msg: err.msg});
              } else {
                res.json({success: true, students: students});
              }
            });
        });
    

    return studentRouter;
  };

module.exports = router;

