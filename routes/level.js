/**
 * Created by ahadrii on 14/08/16.
 */

var express = require('express'),
    levelRouter = express.Router(),
    levelService = require('../services/levelService')();

var router = function(passport) {
    levelRouter.route('/')
        .post(passport.authenticate('jwt', {session: false}), function(req, res) { /** create a level route's **/
                // Inserting the level into the database
                levelService.saveLevel(req, function(err, result) {
                    if (err) {
                      res.json({success: false, msg: err.message});
                    } else {
                      res.json({success: true, msg: result});
                    }
                  });
              })
        .get(passport.authenticate('jwt', {session: false}), function(req, res) { /** Get all the level **/
          levelService.getAllLevels(function(err, levels) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, levels: levels});
              }
            });
        })
        .put(passport.authenticate('jwt', {session: false}), function(req, res) { /** update level  by it's _id **/
          levelService.updateLevel(req, function(err, level) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, level: level});
              }
            });
        })
        .delete(passport.authenticate('jwt', {session: false}), function(req, res) { /** Delete all levels **/
          levelService.delAllLevels(function(err, result) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, msg: result});
              }
            });
        });

    /** Get 1 level by it's _id  **/
    levelRouter.route('/:_id')
        .get(passport.authenticate('jwt', {session: false}), function(req, res) {
            var _id = req.params._id ;
            levelService.getLevelById(_id, function(err, level) {
                if (err) {
                  res.json({success: false, msg: err.message});
                } else {
                  res.json({success: true, level: level});
                }
              });
          })
        .delete(passport.authenticate('jwt', {session: false}), function(req, res) { /** Delete level by it's _id **/
          var _id = req.params._id ;
          levelService.delLevelById(_id, function(err, result) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, msg: result});
              }
            });
        });

    return levelRouter;
  };

module.exports = router;

