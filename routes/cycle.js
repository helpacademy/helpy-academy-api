/**
 * Created by ahadrii on 14/08/16.
 */

var express = require('express'),
    cycleRouter = express.Router(),
    cycleService = require('../services/cycleService')();

var router = function(passport) {
    cycleRouter.route('/')
        .post(passport.authenticate('jwt', {session: false}), function(req, res) { /** create a cycle route's **/
                  // Inserting the cycle into the database
                  cycleService.saveCycle(req, function(err, newCycle) {
                      if (err) {
                        res.json({success: false, msg: err.message});
                      } else {
                        res.json({success: true, msg: newCycle});
                      }
                    });
                })
        .get(passport.authenticate('jwt', {session: false}), function(req, res) { /** Get all the cycles **/
          cycleService.getAllCycles(function(err, cycles) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, cycles: cycles});
              }
            });
        })
        .put(passport.authenticate('jwt', {session: false}), function(req, res) { /** update cycle  by it's _id **/
          cycleService.updateCycle(req, function(err, cycle) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, cycle: cycle});
              }
            });
        })
        .delete(passport.authenticate('jwt', {session: false}), function(req, res) { /** Delete all cycles **/
          cycleService.delAllCycles(function(err, result) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, msg: result});
              }
            });
        });

    /** Get 1 cycle by it's _id  **/
    cycleRouter.route('/:_id')
        .get(passport.authenticate('jwt', {session: false}), function(req, res) {
            var _id = req.params._id ;
            cycleService.getCycleById(_id, function(err, cycle) {
                if (err) {
                  res.json({success: false, msg: err.message});
                } else {
                  res.json({success: true, cycle: cycle});
                }
              });
          })
        .delete(passport.authenticate('jwt', {session: false}), function(req, res) { /** Delete cycle by it's _id **/
          var _id = req.params._id ;
          cycleService.delCycleById(_id, function(err, result) {
              if (err) {
                res.json({success: false, msg: err.message});
              } else {
                res.json({success: true, msg: result});
              }
            });
        });
    
    return cycleRouter;
  };

module.exports = router;

