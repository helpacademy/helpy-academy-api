/**
 * Created by ahadrii on 26/08/16.
 */

var express = require('express'),
    jwt = require('jwt-simple'),
    professorRouter = express.Router(),
    config = require('../config/db.config'),
    professorService = require('../services/professorService')();

var router = function(passport) {
    professorRouter.route('/')
        .put(passport.authenticate('jwt', {session: false}), function(req, res) { // update a professor's route
            professorService.updateProfessor(req, function(err, updatedProfessor) {
                if (err) {
                  return res.json({success: false, msg: err.message});
                } else {
                  var token = jwt.encode(updatedProfessor, config.secret);
                  return res.json({success: true, user: updatedProfessor, token: 'JWT ' + token});
                }
              });
          })
        .post(function(req, res) { // The SignUp route
            if (!req.body.email || !req.body.password || !req.body.fname || !req.body.lname) {
              res.json({success: false, msg: 'S\'il vous plaît remplir tous les champs .'});
            } else {
              professorService.saveProfessor(req, function(err, result) {
                  if (err) {
                    return res.json({success: false, msg: err.message});
                  } else {
                    res.json({success: true, msg: result});
                  }
                });
            }
          })
        .get(passport.authenticate('jwt', {session: false}), function(req, res) { /** Get all the professors **/
            professorService.getAllProfessors(function(err, professors) {
                if (err) {
                  res.json({success: false, msg: err.message});
                } else {
                  res.json({success: true, professors: professors});
                }
              });
          });

    return professorRouter;
  };

module.exports = router;

