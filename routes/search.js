/**
 * Created by ahadrii on 31/08/16.
 */

var express = require('express');
var searchRouter = express.Router();
var searchService = require('../services/searchService')();

var router = function() {
    searchRouter.route('/:keyword')
        .get(function(req, res) {
            var keyword = '^' + req.params.keyword;
            searchService.searchSubject(keyword, function(err, subjects) {
                if (err) {
                  res.json({success: false, msg: err.message});
                } else {
                  res.json({success: true, result: subjects});
                }
              });
          });

    return searchRouter;
  };

module.exports = router;
