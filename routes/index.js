var express = require('express');
var path = require('path');
var indexRouter = express.Router();

var router = function(port) {
  indexRouter.route('/')
      .get(function(req, res) {
        res.status(200).send('Hello! this is daft punk speaking at http://localhost' + port + '/api');
      });

  return indexRouter;
};

module.exports = router;
