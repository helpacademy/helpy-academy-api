/**
 * Created by ahadrii on 30/07/16.
 */

var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt,
    authService = require('../../services/authService')(),
    pino = require('pino')(),
    config = require('../../config/db.config');

module.exports = function(passport) {
    var options = {};
    options.jwtFromRequest = ExtractJwt.fromAuthHeader();
    options.secretOrKey = config.secret;
    passport.use(new JwtStrategy(options, function(jwtPayload, done) {
        authService.findUserById({_id: jwtPayload._id}, function(err, user) {
            if (err) {
              return done(err, false);
            }
            if (user) {
              done(null, user);
            } else {
              done(null, false);
            }
          });
      }));

  };
