/**
 * Created by ahadrii on 29/07/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Creating the Schema
 */
var sessionSchema = new Schema({
    label: {type: String},
    _students: [{type: Schema.Types.ObjectId, ref: 'Student'}],
    _professor: {type: Schema.Types.ObjectId, ref: 'Professor'},
    start: Date,
    end: Date
  });

/**
 * Validate field : _students length && end date value
 */

sessionSchema.path('_students').validate(function(_students) {
    return _students.length < 4;
  }, 'Session should have 4 students im max');

sessionSchema.path('end').validate(function(end) {
    return end >= this.start ;
  }, 'End date should be greater that the start date');

/**
 * Creating the model using the Schema above
 */
var Session = mongoose.model('Session', sessionSchema);

/**
 * Expose the model
 */
module.exports = {
    Session: Session
  };
