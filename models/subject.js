/**
 * Created by ahadrii on 29/07/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var levelService = require('../services/levelService')();
var professorService = require('../services/professorService')();
/**
 * Creating the Schema
 */
var subjectSchema = new Schema({
    label: {type: String, require: 'Please enter the subject title'},
    _level: {type: Schema.Types.ObjectId, ref: 'Level'},
    _cycle: {type: Schema.Types.ObjectId, ref: 'Cycle'},
    _professors: [{type: Schema.Types.ObjectId, ref: 'Professor'}],
    created: {type: Date, default: Date.now}
  });

/**
 * Defining the methods
 */

// generate a hash for the password before any save operation
subjectSchema.pre('save', function(next) {
    var subject = this;

    levelService.addSubject({_id: subject._level, _subjectId: subject._id}, function(err, result) {
        if (err) {
          return next(new Error(err.message));
        } else {
          return next();
        }
      });

    var length = this._professors.length;
    var i = 0 ;
    for (i; i <  length; i++) {
      professorService.addSubject({_id: this._professors[i], _subjectId: subject._id}, function(err, result) {
          if (err) {
            return next(new Error(err.message));
          } else {
            return next();
          }
        });
    }
  });

/**
 * Creating the model using the Schema above
 */
var Subject = mongoose.model('Subject', subjectSchema);

/**
 * Expose the model
 */
module.exports = {
    Subject: Subject
  };
