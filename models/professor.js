/**
 * Created by ahadrii on 29/07/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

/**
 * Creating the Schema
 */
var professorSchema = new Schema({
    fname: {type: String},
    lname: {type: String},
    email: {type: String},
    password: {type: String,
        min: [4, 'Not a valid password']},
    phone: {type: String, default: 'Non disponible'},
    isAdmin: {type: Boolean, default: false},
    date_birth: {type: String},
    status: {type: Boolean, default: false},
    profession: {type: String},
    desc: {type: String},
    _sessions: [{type: Schema.Types.ObjectId, ref: 'Session'}],
    _subjects: [{type: Schema.Types.ObjectId, ref: 'Subject'}],
    created: {type: Date, default: Date.now}
  });

/**
 * Defining the methods
 */

// generate a hash for the password before any save operation
professorSchema.pre('save', function(next) {
    var professor = this;
    if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(10, function(err, salt) {
          if (err) {
            return next(err);
          }
          bcrypt.hash(professor.password, salt, null, function(err, hash) {
              if (err) {
                return next(err);
              }
              professor.password = hash;
              next();
            });
        });
    } else {
      return next();
    }
  });

// checking if professor Schema password is valid
professorSchema.methods.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) {
          return cb(err);
        }
        cb(null, isMatch);

      });
  };

/**
 * Creating the model using the Schema above
 */

var Professor = mongoose.model('Professor', professorSchema);

/**
 * Expose the model
 */
module.exports = {
    Professor: Professor
  };
