/**
 * Created by ahadrii on 29/07/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Creating the Schema
 */
var cycleSchema = new Schema({
    label: {type: String},
    _levels: [{type: Schema.Types.ObjectId, ref: 'Level'}],
    _subjects: [{type: Schema.Types.ObjectId, ref: 'Subject'}],
    created: {type: Date, default: Date.now}
  });

/**
 * Creating the model using the Schema above
 */
var Cycle = mongoose.model('Cycle', cycleSchema);

/**
 * Expose the model
 */
module.exports = {
    Cycle: Cycle
  };
