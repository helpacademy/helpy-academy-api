/**
 * Created by ahadrii on 29/07/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var cycleService = require('../services/cycleService')();

/**
 * Creating the Schema
 */
var levelSchema = new Schema({
    label: {type: String},
    _subjects: [{type: Schema.Types.ObjectId, ref: 'Subject'}],
    _cycle: {type: Schema.Types.ObjectId, ref: 'Cycle'},
    created: {type: Date, default: Date.now}
  });

/**
 * Defining the methods
 */

// generate a hash for the password before any save operation
levelSchema.pre('save', function(next) {
    var level = this;

    cycleService.addLevel({_id: level._cycle, _levelId: level._id}, function(err, result) {
        if (err) {
          return next(new Error(err.message));
        } else {
          return next();
        }
      });
  });




/**
 * Creating the model using the Schema above
 */
var Level = mongoose.model('Level', levelSchema);

/**
 * Expose the model
 */
module.exports = {
    Level: Level
  };
