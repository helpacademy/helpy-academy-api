/**
 * Created by ahadrii on 29/07/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
/**
 * Creating the Schema
 */
var studentSchema = new Schema({
    fname: {type: String, required: 'Please enter your first name'},
    lname: {type: String, required: 'Please enter your last name'},
    email: {type: String, required: 'Please enter your email'},
    phone: {type: String, default: 'Non disponible'},
    date_birth: {type: "String"},
    desc: {type: String},
    password: {type: String, required: 'Please enter a password',
        min: [8, 'Not a valid password']},
    _session: [{type: Schema.Types.ObjectId, ref: 'Session'}],
    created: {type: Date, default: Date.now}
  });

/**
 * Defining the methods
 */

// generate a hash for the password before any save operation
studentSchema.pre('save', function(next) {
    var student = this;
    if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(10, function(err, salt) {
          if (err) {
            return next(err);
          }
          bcrypt.hash(student.password, salt, null, function(err, hash) {
              if (err) {
                return next(err);
              }
              student.password = hash;
              next();
            });
        });
    } else {
      return next();
    }
  });

// checking if password is valid
studentSchema.methods.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) {
          return cb(err);
        }
        cb(null, isMatch);

      });
  };

/**
 * Creating the model using the Schema above
 */

var Student = mongoose.model('Student', studentSchema);

/**
 * Expose the model
 */
module.exports = {
    Student: Student
  };
