/**
 * Created by ahadrii on 29/07/16.
 */

module.exports = function() {
    var client = './src/client/';
    var server = './src/server/';
    var configPath = './config/';

    var config = {
        server: server,
        alljs: [server + '**/*.js', configPath + '*.js', './*.js'],

        index: client + '/views/coming-soon.html',
        css: [client + 'css/*.css', client + 'css/**/*.css'],
        js: [client + 'js/**/*.js', client + '*.js'],
        views: client + '/views/',
        defaultPort: 7203,
        nodeServer: './src/server/server.js',
        browserReloadDelay: 1000,
        bower: {
            json: require('./bower.json'),
            directory: './bower_components/',
            ignorePath: '../..'
          }
      };

    /** Wiredep settings ***/
    // config.getWiredepDefaultOptions = function() {
    //   var options = {
    //       bowerJson: config.bower.json,
    //       directory: config.bower.directory,
    //       ignorePath: config.bower.ignorePath
    //     };
    //   return options;
    // };

    return config;
  };

